<!DOCTYPE html>
<html lang='vn'> 
<head><meta charset='UTF-8'></head> 
<title>Search</title>
<body>
    <fieldset style='width: 500px; height: 550px; border:#2E8BC0 solid; margin: auto'>
    <?php      
        session_start();
        $_SESSION = $_POST;

        if ($_SERVER['REQUEST_METHOD'] == "POST") {
            header("Location: ./Register.php");
        }
    ?>

<form action='' method="post" enctype="multipart/form-data">
        <div style='padding-top:30px'>
            <span class = 'form-group required control-label' style='background-color:#FEAFA2;
                        padding:5px 10px;
                        margin:20px 20px'>Phân khoa</span>
            <select name="depart" style='border:2px solid #FEAFA2; padding:5px 0px'>
            	<?php
                $depart = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
                foreach ($depart as $key => $value) { ?>
                	<option value="<?=$value?>"><?=$value ?></option>
                <?php }
                ?>
            </select>
        </div>

        <div style='padding-top:30px'>
            <span style='background-color:#FEAFA2;
                        padding:5px 10px;
                        margin:20px 20px'>Từ khóa</span>
            <input name = 'key' style='border:2px solid #FEAFA2;
                            padding:5px 0px;
                            margin:0px 20px'>
        </div>
        
        <div style='margin-top:20px;margin-left:100px'>
            <button class = 'search' style='background-color:#FEA6B6;
                        padding:10px 20px;
                        border-radius:8px;
                        border-color:#FEA6B6'>Tìm kiếm</button>
        </div>
    
        <div class="result">
                <div>
                    <h4>Số sinh viên tìm thấy: xxx</h4>

                <div style='margin-top:20px;margin-left:100px'>
                    <button class = 'add_student' style='background-color:#FEA6B6;
                        padding: 10px 24px;
                        border-radius: 10px;
                        border-color:#FEA6B6'>Thêm</button>
                </div>
                </div>
        </div>
        
        <div class="list-student">
            <table>
                <tr>
                    <th>No</th>
                    <th>Tên sinh viên</th>
                    <th>Khoa</th>
                    <th>Action</th>
                </tr>
                <tr>
                    <td>1</td>
                    <td style = 'padding: 10px'>Nguyễn Văn A</td>
                    <td style = 'padding: 10px'>Khoa học máy tính</td>
                    <td>
                        <button class="btn-action" style='background-color:#FEA6B6;
                        padding: 8px 20px border-radius: 10px; border-color:#FEA6B6'>Xóa</button>
                        <button class="btn-action" style='background-color:#FEA6B6;
                        padding: 8px 20px border-radius: 10px; border-color:#FEA6B6'>Sửa</button>
                    </td>
                </tr>

                <tr>
                    <td>2</td>
                    <td style = 'padding: 10px'>Trần Thị B</td>
                    <td style = 'padding: 10px'>Khoa học máy tính</td>
                    <td>
                        <button class="btn-action" style='background-color:#FEA6B6;
                        padding: 8px 20px border-radius: 10px; border-color:#FEA6B6'>Xóa</button>
                        <button class="btn-action" style='background-color:#FEA6B6;
                        padding: 8px 20px border-radius: 10px; border-color:#FEA6B6'>Sửa</button>
                    </td>
                </tr>

                <tr>
                    <td>3</td>
                    <td style = 'padding: 10px'>Nguyễn Hoàng C</td>
                    <td style = 'padding: 10px'>Khoa học vật liệu</td>
                    <td>
                        <button class="btn-action" style='background-color:#FEA6B6;
                        padding: 8px 20px border-radius: 10px; border-color:#FEA6B6'>Xóa</button>
                        <button class="btn-action" style='background-color:#FEA6B6;
                        padding: 8px 20px border-radius: 10px; border-color:#FEA6B6'>Sửa</button>
                    </td>
                </tr>

                <tr>
                    <td>4</td>
                    <td style = 'padding: 10px'>Đinh Quang D</td>
                    <td style = 'padding: 10px'>Khoa học vật liệu</td>
                    <td>
                        <button class="btn-action" style='background-color:#FEA6B6;
                        padding: 8px 20px border-radius: 10px; border-color:#FEA6B6'>Xóa</button>
                        <button class="btn-action" style='background-color:#FEA6B6;
                        padding: 8px 20px border-radius: 10px; border-color:#FEA6B6'>Sửa</button>
                    </td>
                </tr>
            </table>
        </div>


        
</form>
</style>
</body>
</html>
